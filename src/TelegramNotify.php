<?php

namespace TelegramNotify;

use GuzzleHttp\Client;

class TelegramNotify
{
    protected $proxy;
    protected $botAccess;
    protected $url;
    protected $debug = false;

    public function setDebug(bool $debug){
        $this->debug = $debug;
    }

    public function setProxy(string $proxy){
        $this->proxy = $proxy;
    }

    public function setBotAccess(string $botAccess){

        $this->botAccess = $botAccess;

        $this->buildUrl();
    }

    private function buildUrl(){
        $this->url = "https://api.telegram.org/bot".$this->botAccess."/";
    }

    public function send(string $message, $dialogIDs){

        try{

            if(!is_array($dialogIDs)){
                $dialogIDs = [$dialogIDs];
            }

            $httpClient = new Client();

            $options['headers'] = [
                "Content-Type: application/json"
            ];

            $options['json'] = [
                "chat_id" => 0,
                "text" => $message,
                "disable_web_page_preview" => true,
                "parse_mode" => "HTML"
            ];

            if(!is_null($this->proxy)){
                $options['proxy'] = $this->proxy;
            }

            $result = ["status" => "Success", "errors" => []];

            foreach ($dialogIDs as $dialogID) {

                $options['json']['chat_id'] = $dialogID;

                if($this->debug){
                    $result['request'] = $options;
                }

                try{

                    $res = $httpClient->request("POST", $this->url."sendMessage", $options);

                    $code = $res->getStatusCode();

                    if($code != 200){
                        $result['errors'][] = "Response code is {$code}";
                    }

                }catch(\Exception $e){
                    $result['errors'][] = $e->getMessage();
                }
            }

            if(count($result['errors']) == count($dialogIDs)){
                $result['status'] = "Error";
            }else if(count($result['errors']) > 0 && count($result['errors']) < count($dialogIDs)){
                $result['status'] = "Warning";
            }

            return $result;

        }catch(\Exception $e){
            return ["status" => "FatalError", "message" => $e->getMessage()];
        }
    }
}
